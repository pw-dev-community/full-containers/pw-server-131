#!/bin/sh
echo ""
echo ""
echo "###########################################################################################"
echo "BORODA Repack. Starting servers..."
echo "###########################################################################################"
echo ""
echo ""

./gs gs01  >gs01.log  2>&1 &
sleep 15

for ((  i = 1 ;  i <= 5;  i++  ))
do
        ./gs arena0$i  >/dev/null 2>&1 &
done
echo "starting Arena's, sleeping 10"
sleep 10


for ((  e = 1 ;  e <= 31;  e++  ))
do
        if test $e -le 9
        then
                ./gs is0$e  >/dev/null 2>&1 &
        else
                ./gs is$e  >/dev/null 2>&1 &
        fi
sleep 5
done
echo "starting Is's, sleeping 10"
sleep 10


for ((  o = 1 ;  o <= 6;  o++  ))
do
        ./gs bg0$o  >/dev/null 2>&1 &
done
echo "starting Bg's, sleeping 5"
sleep 5


echo "Finish starting worlds!"
