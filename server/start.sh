#!/bin/sh
PW_PATH=/server
if [ ! -d $PW_PATH/logs ]; then
mkdir $PW_PATH/logs
fi
echo "=================================================================="
echo "=                            1.3.1                               ="
echo "=                                                                ="
echo "=================================================================="
echo ""
echo ""
#echo "=== IWEB AND ROLE MANAGEMENT ==="
#/usr/local/jakarta-tomcat-5.5.9/bin/startup.sh
#sleep 5
#echo "=== DONE! ==="
#echo ""
echo "=== LOGSERVICE ==="
cd $PW_PATH/logservice; ./logservice logservice.conf >$PW_PATH/logs/logservice.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== UNIQUENAMED ==="
cd $PW_PATH/uniquenamed; ./uniquenamed gamesys.conf >$PW_PATH/logs/uniquenamed.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== AUTH ==="
cd $PW_PATH/authd/build/; ./authd &
sleep 3
echo "=== DONE! ==="
echo ""
echo "=== GAMEDBD ==="
cd $PW_PATH/gamedbd; ./gamedbd gamesys.conf >$PW_PATH/logs/gamedbd.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== GACD ==="
cd $PW_PATH/gacd; ./gacd gamesys.conf >$PW_PATH/logs/gacd.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== GFACTIOND ==="
cd $PW_PATH/gfactiond; ./gfactiond gamesys.conf >$PW_PATH/logs/gfactiond.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== GDELIVERYD ==="
cd $PW_PATH/gdeliveryd; ./gdeliveryd gamesys.conf >$PW_PATH/logs/gdeliveryd.log &
sleep 1
echo "=== DONE! ==="
echo ""
echo "=== GLINKD ==="
cd $PW_PATH/glinkd; ./glinkd gamesys.conf 1 >$PW_PATH/logs/glink.log &
cd $PW_PATH/glinkd; ./glinkd gamesys.conf 2 >$PW_PATH/logs/glink2.log &
cd $PW_PATH/glinkd; ./glinkd gamesys.conf 3 >$PW_PATH/logs/glink3.log &
cd $PW_PATH/glinkd; ./glinkd gamesys.conf 4 >$PW_PATH/logs/glink4.log &
sleep 3
echo "=== DONE! ==="
echo ""
echo "=== MAIN WORLD ==="
cd $PW_PATH/gamed; ./gs gs01 >$PW_PATH/logs/game1.log &
sleep 30
