#!/bin/sh
chmod 755 -R /server/authd
chmod 755 -R /server/gacd
chmod 755 -R /server/gamed
chmod 755 -R /server/gamedbd
chmod 755 -R /server/gdeliveryd
chmod 755 -R /server/gfactiond
chmod 755 -R /server/glinkd
chmod 755 -R /server/logs
chmod 755 -R /server/logservice
chmod 755 -R /server/pw
chmod 777 -R /server/tmp
chmod 755 -R /server/uniquenamed
chmod 755 -R /server/stop.sh
chmod 755 -R /server/start.sh
ldconfig
