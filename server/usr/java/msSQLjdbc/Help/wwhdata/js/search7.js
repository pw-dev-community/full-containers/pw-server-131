function  Book_Search(ParamSearchWord)
{
  var  Results = null;


  if (ParamSearchWord == "class.forname")
    Results = new Array("jquikstart2.html","usejdbc3.html");
  else if (ParamSearchWord == "return")
    Results = new Array("jdbcdesign2.html","jdbcdesign3.html","jdbcdesign4.html","jdbcsqlsrv3.html");
  else if (ParamSearchWord == "locator")
    Results = new Array("jdbcdesign4.html");
  else if (ParamSearchWord == "thus")
    Results = new Array("jdbcdesign5.html","jdbcdesign6.html");
  else if (ParamSearchWord == "designing")
    Results = new Array("jdbcdesign6.html","jdbcdesign.html");
  else if (ParamSearchWord == "often")
    Results = new Array("jdbcdesign6.html","jdbcdesign7.html","scalarfn.html");
  else if (ParamSearchWord == "reused")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "located")
    Results = new Array("jdbcdesign7.html","jdbcsqlsrv9.html","usejdbc8.html");
  else if (ParamSearchWord == "identifies")
    Results = new Array("jdbcdesign7.html");
  else if (ParamSearchWord == "islast")
    Results = new Array("jdbcsupp11.html");
  else if (ParamSearchWord == "jdbcsupp14")
    Results = new Array("jdbcsupp14.html");
  else if (ParamSearchWord == "setlong")
    Results = new Array("jdbcsupp3.html","jdbcsupp9.html");
  else if (ParamSearchWord == "gettypemap")
    Results = new Array("jdbcsupp4.html");
  else if (ParamSearchWord == "settransactionisolation")
    Results = new Array("jdbcsupp4.html");
  else if (ParamSearchWord == "jdbcsupp5")
    Results = new Array("jdbcsupp5.html");
  else if (ParamSearchWord == "machines")
    Results = new Array("jdbcsupp.html");
  else if (ParamSearchWord == "8000")
    Results = new Array("getinfoapp.html");
  else if (ParamSearchWord == "intranets")
    Results = new Array("usejdbc.html");
  else if (ParamSearchWord == "cont")
    Results = new Array("jdbcsqlsrv3.html");
  else if (ParamSearchWord == "sendstringparametersasunicode")
    Results = new Array("jdbcsqlsrv3.html");
  else if (ParamSearchWord == "master")
    Results = new Array("jdbcsqlsrv9.html");
  else if (ParamSearchWord == "assigns")
    Results = new Array("usejdbc4.html");
  else if (ParamSearchWord == "menus")
    Results = new Array("Preface2.html");
  else if (ParamSearchWord == "database")
    Results = new Array("jquikstart2.html","jdbcdesign2.html","jdbcdesign4.html","jdbcdesign5.html","jdbcdesign6.html","jdbcdesign7.html","jdbcsqlsrv.html","jdbcdesign.html","jdbcsqlsrv3.html","jdbcsqlsrv4.html","jdbcsqlsrv9.html","Preface.html","usejdbc3.html","usejdbc4.html","usejdbc7.html");
  else if (ParamSearchWord == "listening")
    Results = new Array("jquikstart2.html");
  else if (ParamSearchWord == "previous")
    Results = new Array("jquikstart2.html","jdbcdesign2.html","jdbcdesign3.html","jdbcdesign4.html","jdbcdesign5.html","jdbcdesign6.html","jdbcdesign7.html","jdbcdesign8.html","scalarfn2.html","scalarfn3.html","scalarfn4.html","scalarfn5.html","jdbcsqlsrv.html","jdbcsupp10.html","jdbcsupp11.html","jdbcsupp12.html","jdbcsupp13.html","jdbcsupp14.html","jdbcsupp15.html","jdbcsupp16.html","jdbcsupp17.html","jdbcsupp18.html","jdbcsupp2.html","jdbcsupp3.html","jdbcsupp4.html","jdbcsupp5.html","jdbcsupp6.html","jdbcsupp7.html","jdbcsupp8.html","jdbcsupp9.html","jdbcsupp.html","getinfoapp.html","usejdbc.html","jdbcdesign.html","jdbcsqlsrv2.html","jdbcsqlsrv3.html","jdbcsqlsrv4.html","jdbcsqlsrv5.html","jdbcsqlsrv6.html","jdbcsqlsrv7.html","jdbcsqlsrv8.html","jdbcsqlsrv9.html","jquikstart.html","Preface.html","usejdbc2.html","usejdbc3.html","usejdbc4.html","usejdbc5.html","usejdbc6.html","usejdbc7.html","usejdbc8.html","scalarfn.html","Preface2.html");
  else if (ParamSearchWord == "avoiding")
    Results = new Array("jdbcdesign2.html","jdbcdesign6.html");
  else if (ParamSearchWord == "known")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "performing")
    Results = new Array("jdbcdesign2.html","jdbcdesign6.html");
  else if (ParamSearchWord == "100")
    Results = new Array("jdbcdesign4.html");
  else if (ParamSearchWord == "schema")
    Results = new Array("jdbcdesign4.html");
  else if (ParamSearchWord == "least")
    Results = new Array("jdbcdesign4.html","jdbcdesign6.html");
  else if (ParamSearchWord == "server-side")
    Results = new Array("jdbcdesign5.html","jdbcsqlsrv3.html");
  else if (ParamSearchWord == "memory")
    Results = new Array("jdbcdesign5.html","jdbcdesign6.html","jdbcsqlsrv3.html");
  else if (ParamSearchWord == "issue")
    Results = new Array("jdbcdesign5.html");
  else if (ParamSearchWord == "affect")
    Results = new Array("jdbcdesign5.html");
  else if (ParamSearchWord == "throughout")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "rs.updateint")
    Results = new Array("jdbcdesign7.html");
  else if (ParamSearchWord == "exact")
    Results = new Array("jdbcdesign7.html");
  else if (ParamSearchWord == "cos")
    Results = new Array("scalarfn3.html");
  else if (ParamSearchWord == "timestampadd")
    Results = new Array("scalarfn3.html");
  else if (ParamSearchWord == "getasciistream")
    Results = new Array("jdbcsupp11.html");
  else if (ParamSearchWord == "bigdecimal")
    Results = new Array("jdbcsupp11.html","jdbcsupp3.html","jdbcsupp9.html");
  else if (ParamSearchWord == "getcolumnclassname")
    Results = new Array("jdbcsupp12.html");
  else if (ParamSearchWord == "jdbcsupp15")
    Results = new Array("jdbcsupp15.html");
  else if (ParamSearchWord == "jdbcsupp6")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "getmaxrowsize")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "getmaxstatementlength")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsresultsettype")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "quantifieds")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "getpropertyinfo")
    Results = new Array("jdbcsupp8.html");
  else if (ParamSearchWord == "bigint")
    Results = new Array("getinfoapp.html","jdbcsqlsrv5.html");
  else if (ParamSearchWord == "environment")
    Results = new Array("usejdbc.html","usejdbc4.html","usejdbc6.html");
  else if (ParamSearchWord == "netaddress")
    Results = new Array("jdbcsqlsrv3.html");
  else if (ParamSearchWord == "message")
    Results = new Array("jdbcsqlsrv3.html","jdbcsqlsrv9.html","usejdbc7.html");
  else if (ParamSearchWord == "successfully")
    Results = new Array("jdbcsqlsrv9.html");
  else if (ParamSearchWord == "operating")
    Results = new Array("Preface.html","Preface2.html");
  else if (ParamSearchWord == "variable")
    Results = new Array("jquikstart2.html");
  else if (ParamSearchWord == "java")
    Results = new Array("jquikstart2.html","jdbcsupp.html","usejdbc.html","usejdbc2.html","usejdbc4.html","usejdbc6.html");
  else if (ParamSearchWord == "when")
    Results = new Array("jquikstart2.html","jdbcdesign2.html","jdbcdesign3.html","jdbcdesign4.html","jdbcdesign5.html","jdbcdesign6.html","jdbcdesign7.html","jdbcsqlsrv.html","jdbcsupp4.html","jdbcsqlsrv3.html","jdbcsqlsrv8.html","jdbcsqlsrv9.html","usejdbc4.html","scalarfn.html");
  else if (ParamSearchWord == "secret")
    Results = new Array("jquikstart2.html","jdbcsqlsrv4.html","usejdbc3.html");
  else if (ParamSearchWord == "mandated")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "unwanted")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "quickly")
    Results = new Array("jdbcdesign5.html");
  else if (ParamSearchWord == "span")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "1023")
    Results = new Array("scalarfn2.html");
  else if (ParamSearchWord == "cot")
    Results = new Array("scalarfn3.html");
  else if (ParamSearchWord == "executable")
    Results = new Array("scalarfn5.html","usejdbc8.html");
  else if (ParamSearchWord == "getschemaname")
    Results = new Array("jdbcsupp12.html");
  else if (ParamSearchWord == "issearchable")
    Results = new Array("jdbcsupp12.html");
  else if (ParamSearchWord == "jdbcsupp16")
    Results = new Array("jdbcsupp16.html");
  else if (ParamSearchWord == "click")
    Results = new Array("jdbcsupp2.html","Preface2.html");
  else if (ParamSearchWord == "getmaxbinaryliterallength")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "getmaxcolumnsinselect")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "manipulationtransactions")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsselectforupdate")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "jdbcsupp7")
    Results = new Array("jdbcsupp7.html");
  else if (ParamSearchWord == "recently")
    Results = new Array("jdbcsupp8.html");
  else if (ParamSearchWord == "varbinary")
    Results = new Array("getinfoapp.html","jdbcsqlsrv5.html");
  else if (ParamSearchWord == "incorporate")
    Results = new Array("usejdbc.html");
  else if (ParamSearchWord == "instance1")
    Results = new Array("jdbcsqlsrv4.html");
  else if (ParamSearchWord == "repeated")
    Results = new Array("jdbcsqlsrv9.html","Preface2.html");
  else if (ParamSearchWord == "employeedb")
    Results = new Array("usejdbc4.html");
  else if (ParamSearchWord == "documentation")
    Results = new Array("usejdbc6.html","usejdbc7.html","usejdbc8.html");
  else if (ParamSearchWord == "sorting")
    Results = new Array("usejdbc6.html");
  else if (ParamSearchWord == "enter")
    Results = new Array("Preface2.html");
  else if (ParamSearchWord == "establish")
    Results = new Array("jquikstart2.html","jdbcdesign6.html","usejdbc4.html");
  else if (ParamSearchWord == "windows")
    Results = new Array("jquikstart2.html","usejdbc8.html");
  else if (ParamSearchWord == "compared")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "null")
    Results = new Array("jdbcdesign2.html","getinfoapp.html");
  else if (ParamSearchWord == "consider")
    Results = new Array("jdbcdesign2.html","jdbcdesign7.html");
  else if (ParamSearchWord == "less")
    Results = new Array("jdbcdesign5.html","jdbcdesign8.html");
  else if (ParamSearchWord == "concurrency")
    Results = new Array("jdbcdesign5.html","jdbcdesign6.html","jdbcdesign.html","jdbcsqlsrv8.html");
  else if (ParamSearchWord == "scroll")
    Results = new Array("jdbcdesign5.html","jdbcsupp4.html");
  else if (ParamSearchWord == "closing")
    Results = new Array("jdbcdesign6.html","Preface2.html");
  else if (ParamSearchWord == "extremely")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "existing")
    Results = new Array("jdbcdesign6.html","usejdbc.html","jdbcsqlsrv9.html");
  else if (ParamSearchWord == "updatexxx")
    Results = new Array("jdbcdesign7.html");
  else if (ParamSearchWord == "isnullable")
    Results = new Array("jdbcsupp12.html");
  else if (ParamSearchWord == "jdbcsupp17")
    Results = new Array("jdbcsupp17.html");
  else if (ParamSearchWord == "taken")
    Results = new Array("jdbcsupp2.html");
  else if (ParamSearchWord == "nullsaresortedatend")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsdatamanipulation")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsminimumsqlgrammar")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "jdbcsupp8")
    Results = new Array("jdbcsupp8.html");
  else if (ParamSearchWord == "made")
    Results = new Array("usejdbc4.html","usejdbc7.html");
  else if (ParamSearchWord == "sources")
    Results = new Array("jquikstart2.html","jdbcsqlsrv2.html","jdbcsqlsrv3.html","usejdbc2.html","usejdbc4.html","usejdbc5.html","usejdbc6.html");
  else if (ParamSearchWord == "include")
    Results = new Array("jquikstart2.html","jdbcdesign4.html","usejdbc4.html");
  else if (ParamSearchWord == "mssqlserver2000jdbc")
    Results = new Array("jquikstart2.html");
  else if (ParamSearchWord == "instances")
    Results = new Array("jquikstart2.html","jdbcsqlsrv4.html");
  else if (ParamSearchWord == "username")
    Results = new Array("jquikstart2.html","scalarfn3.html");
  else if (ParamSearchWord == "synchronize")
    Results = new Array("jquikstart2.html","jdbcdesign2.html","jdbcdesign3.html","jdbcdesign4.html","jdbcdesign5.html","jdbcdesign6.html","jdbcdesign7.html","jdbcdesign8.html","scalarfn2.html","scalarfn3.html","scalarfn4.html","scalarfn5.html","jdbcsqlsrv.html","jdbcsupp10.html","jdbcsupp11.html","jdbcsupp12.html","jdbcsupp13.html","jdbcsupp14.html","jdbcsupp15.html","jdbcsupp16.html","jdbcsupp17.html","jdbcsupp18.html","jdbcsupp2.html","jdbcsupp3.html","jdbcsupp4.html","jdbcsupp5.html","jdbcsupp6.html","jdbcsupp7.html","jdbcsupp8.html","jdbcsupp9.html","jdbcsupp.html","getinfoapp.html","usejdbc.html","jdbcdesign.html","jdbcsqlsrv2.html","jdbcsqlsrv3.html","jdbcsqlsrv4.html","jdbcsqlsrv5.html","jdbcsqlsrv6.html","jdbcsqlsrv7.html","jdbcsqlsrv8.html","jdbcsqlsrv9.html","jquikstart.html","Preface.html","usejdbc2.html","usejdbc3.html","usejdbc4.html","usejdbc5.html","usejdbc6.html","usejdbc7.html","usejdbc8.html","scalarfn.html","Preface2.html");
  else if (ParamSearchWord == "preparedstatement")
    Results = new Array("jdbcdesign2.html","jdbcdesign5.html","jdbcsupp4.html","jdbcsupp9.html");
  else if (ParamSearchWord == "discussion")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "natively")
    Results = new Array("jdbcdesign2.html");
  else if (ParamSearchWord == "amount")
    Results = new Array("jdbcdesign6.html","jdbcsqlsrv3.html");
  else if (ParamSearchWord == "fragment")
    Results = new Array("jdbcdesign7.html");
  else if (ParamSearchWord == "quarter")
    Results = new Array("scalarfn3.html");
  else if (ParamSearchWord == "table-reference")
    Results = new Array("scalarfn4.html");
  else if (ParamSearchWord == "customers.name")
    Results = new Array("scalarfn4.html");
  else if (ParamSearchWord == "precompiled")
    Results = new Array("scalarfn5.html");
  else if (ParamSearchWord == "jdbcsupp18")
    Results = new Array("jdbcsupp18.html");
  else if (ParamSearchWord == "getmaxusernamelength")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "ownupdatesarevisible")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsextendedsqlgrammar")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "supportsgroupbyunrelated")
    Results = new Array("jdbcsupp6.html");
  else if (ParamSearchWord == "jdbcsupp9")
    Results = new Array("jdbcsupp9.html");
  else if (ParamSearchWord == "technology")
    Results = new Array("usejdbc.html");
  else if (ParamSearchWord == "configuration")
    Results = new Array("usejdbc6.html");
  else if (ParamSearchWord == "specifically")
    Results = new Array("usejdbc8.html");
  else if (ParamSearchWord == "keys")
    Results = new Array("Preface2.html");
  else if (ParamSearchWord == "named")
    Results = new Array("jquikstart2.html","jdbcsqlsrv3.html","jdbcsqlsrv4.html");
  else if (ParamSearchWord == "value")
    Results = new Array("jquikstart2.html","jdbcdesign5.html","jdbcdesign7.html","scalarfn2.html","jdbcsqlsrv3.html","usejdbc3.html");
  else if (ParamSearchWord == "getcolumns")
    Results = new Array("jdbcdesign2.html","jdbcdesign7.html","jdbcsupp6.html");
  else if (ParamSearchWord == "establishing")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "far")
    Results = new Array("jdbcdesign6.html");
  else if (ParamSearchWord == "power")
    Results = new Array("scalarfn3.html");
  else if (ParamSearchWord == "rand")
    Results = new Array("scalarfn3.html");

  return Results;
}
