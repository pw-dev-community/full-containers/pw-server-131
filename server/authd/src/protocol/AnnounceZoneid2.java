package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;

public final class AnnounceZoneid2 extends Protocol
{
	public int	zoneid;
	public int	aid;
	public byte	blreset;

	public AnnounceZoneid2()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(zoneid);
		os.marshal(aid);
		os.marshal(blreset);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		zoneid = os.unmarshal_int();
		aid = os.unmarshal_int();
		blreset = os.unmarshal_byte();
		return os;
	}

	public Object clone()
	{
		try
		{
			AnnounceZoneid2 o = (AnnounceZoneid2)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		GAuthServer aum = GAuthServer.GetInstance();
		Integer _zoneid,_aid;
		_zoneid=new Integer(zoneid);
		_aid=new Integer(aid);
		if (blreset!=0)
		{
			GAuthServer.GetLog().info("AnnounceZoneid2, clear all online records on zone "+_zoneid);
			storage.clearOnlineRecords(_zoneid,_aid);
		}

		Session sessionOld = aum.GetSessionbyZoneid( _zoneid );
		if( null != sessionOld ){
			GAuthServer.GetLog().info( "AnnounceZoneid2 ERROR, zoneid already exists. New session will replace Old!!! zoneid="+_zoneid );
			GAuthServer.zonemap.remove(sessionOld);
			GAuthServer.aidmap.remove(sessionOld);  
				
		}
		aum.zonemap.put(session,_zoneid);
		aum.aidmap.put(session,_aid);
		GAuthServer.GetLog().info("AnnounceZoneid2, zone "+_zoneid+"  aid "+_aid+" announced.");
	}

}
