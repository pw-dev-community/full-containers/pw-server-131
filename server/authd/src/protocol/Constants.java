package protocol;
public final class Constants
{
	public final static int ERR_ACCOUNTEMPTY = 23;
	public final static int ERR_KICKOUT = 32;
	public final static int ERR_MULTILOGIN = 10;
	public final static int ERR_SUCCESS = 0;
	public final static int ERR_COMMUNICATION = 8;
	public final static int ERR_INVALID_ACCOUNT = 2;
	public final static int ERR_LOGOUT_FAIL = 12;


	public final static int _SID_INVALID = 0;


	//forbid types
	public final static int PRV_FORCE_OFFLINE = 100;  //强制玩家下线，并禁止在一定时间上线
	public final static int PRV_FORBID_TALK = 101;  //禁言
	public final static int PRV_FORBID_TRADE = 102;  //禁止玩家间、玩家与NPC交易
	public final static int PRV_FORBID_SELL = 103;  //禁卖
	public final static int PRV_FORBID_SELLPOINT = 104;  //禁卖
}
