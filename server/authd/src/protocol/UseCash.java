package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.IO.PollIO;
import com.goldhuman.account.storage;

public final class UseCash extends Protocol
{
	public int	userid;
	public int	zoneid;
	public int	aid;
	public int	point;
	public int	cash;

	public UseCash()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(userid);
		os.marshal(zoneid);
		os.marshal(aid);
		os.marshal(point);
		os.marshal(cash);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		userid = os.unmarshal_int();
		zoneid = os.unmarshal_int();
		aid = os.unmarshal_int();
		point = os.unmarshal_int();
		cash = os.unmarshal_int();
		return os;
	}

	public Object clone()
	{
		try
		{
			UseCash o = (UseCash)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		GAuthServer.GetLog().info("UseCash: userid="+userid+",zoneid="+zoneid+",aid="+aid+
				",point="+point+",cash="+cash );

		GAuthServer aum=GAuthServer.GetInstance();
		Session s = aum.GetSessionbyZoneid(new Integer(zoneid));
		UseCash_Re re = (UseCash_Re) Protocol.Create("USECASH_RE");
		re.userid = userid;
		re.zoneid = zoneid;
		if( null == s )
		{
			re.retcode = -6;
			manager.Send( session, re );
			return;
		}

		GetAddCashSNArg rpcarg = new GetAddCashSNArg();
		rpcarg.userid = userid;
		rpcarg.zoneid = zoneid;
		GetAddCashSN rpc = (GetAddCashSN)Rpc.Call("GetAddCashSN", rpcarg);
		if( aum.Send( s,rpc ) )
		{
			PollIO.WakeUp();
			re.retcode = 0;
			aum.SetUseCashSession( manager, session );
		}
		else
		{
			re.retcode = -13;
			manager.Send( session, re );
		}
	}

}
