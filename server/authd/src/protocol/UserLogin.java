package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
import java.util.*;
import java.lang.Thread;
public final class UserLogin extends Rpc
{
	public Session GetSessionbyZoneid(Integer zoneid)
	{
		GAuthServer aum=GAuthServer.GetInstance();
		if (!aum.zonemap.containsValue(zoneid)) return null;
		Set entryset = aum.zonemap.entrySet();
		Iterator it=entryset.iterator();
		Map.Entry map_entry;
		while (it.hasNext())
		{
			map_entry =(Map.Entry) it.next();
			if (((Integer)map_entry.getValue()).intValue() == zoneid.intValue())
				return (Session)map_entry.getKey();
		}
		return null;
	}
	public void Server(Data argument, Data result, Manager manager, Session session) throws ProtocolException
	{
		UserLoginArg arg = (UserLoginArg)argument;
		UserLoginRes res = (UserLoginRes)result;

		GAuthServer aum=GAuthServer.GetInstance();
        if (aum.zonemap.get(session)==null) { res.retcode=-1; return; }
        if (aum.aidmap.get(session)==null) { res.retcode=-1; return; }		
		
		int zoneid=((Integer)aum.zonemap.get(session)).intValue();
		int aid=((Integer)aum.aidmap.get(session)).intValue();

		res.remain_playtime=0;
		res.free_time_left=0;
		res.free_time_end=0;
		res.creatime = 0;

		res.func=0;
		res.funcparm=0;
		res.adduppoint = 0;
		res.soldpoint = 0;

		res.creatime = storage.acquireUserCreatime(new Integer(arg.userid));

		Object[] param=new Object[]{new Integer(zoneid),new Integer(arg.localsid),new Integer(arg.blkickuser)};
		if (!storage.recordUserOnline(param,new Integer(arg.userid),new Integer(aid)))
		{ res.retcode=Constants.ERR_COMMUNICATION; return; } 
		if (!(param[0]==null || param[1]==null || param[2]==null))
		{
			//identical user has login,justify whether to kickout user
			if (((Integer)param[0]).intValue() != zoneid || ((Integer)param[1]).intValue() != arg.localsid)
			{
				if (((Integer)param[2]).intValue()==1)
				{
					KickoutUser ku=(KickoutUser) super.Create("KICKOUTUSER");
					ku.userid=arg.userid;
					ku.localsid=((Integer)param[1]).intValue();
					ku.cause=Constants.ERR_KICKOUT;	
					GAuthServer.GetLog().info("Send Kickout userid="+ku.userid+" sid="+ku.localsid);
					Session sid= GetSessionbyZoneid((Integer)param[0]);
					if (sid!= null)
						aum.Send( sid,ku );
					else
					{
						GAuthServer.GetLog().info("Error: kickout user "+ku.userid+" failed.");
					}
				}
				else
				{
					res.retcode = Constants.ERR_MULTILOGIN;
					return;
				}
			}
		}
		res.retcode = Constants.ERR_SUCCESS;
		Object[] prv=storage.acquireUserPrivilege(new Integer(arg.userid),new Integer(zoneid));
		res.blIsGM = ( prv!=null && prv.length!=0 ) ? (byte)1:(byte)0;
		GAuthServer.GetLog().info("UserLogin:userid="+arg.userid+",sid="+arg.localsid+",aid="+aid+",zoneid="+zoneid+",remaintime="+res.remain_playtime+",free_time_left="+res.free_time_left+",free_time_end="+res.free_time_end+",func="+res.func+",funcparm="+res.funcparm+",creatime="+res.creatime+",adduppoint="+res.adduppoint+",soldpoint="+res.soldpoint);
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		UserLoginArg arg = (UserLoginArg)argument;
		UserLoginRes res = (UserLoginRes)result;
	}

	public void OnTimeout()
	{
	}

}
