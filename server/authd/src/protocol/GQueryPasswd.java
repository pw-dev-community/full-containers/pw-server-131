package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
import java.util.Calendar;
import java.util.Date;

public final class GQueryPasswd extends Rpc
{

	private static void initReason() {
		if( forbidIPReason == null || lockReason == null )
		{
		try {
			forbidIPReason = new String("此IP段不符合设定允许的范围，禁止登录。").getBytes("UTF-16LE");
			lockReason = new String("此帐号已被手机锁定，禁止登录。").getBytes("UTF-16LE");
		} catch(Exception ex)
		{
			forbidIPReason = new String("forbidden ip address").getBytes();
			lockReason = new String("account locked by mobile").getBytes();
		}
		}
	}
	private static byte[] forbidIPReason = null;
	private static byte[] lockReason = null;
	private static int bswap(int i)
    {
        int b0 = i&0xff;
        int b1 = (i>>8)&0xff;
        int b2 = (i>>16)&0xff;
        int b3 = (i>>24)&0xff;
        return 0xff000000&((b0)<<24) | 0xff0000&((b1)<<16) | 0xff00&((b2)<<8) | 0xff&b3;
    }
	/*
	private static int bswap(int i)
	{
		byte b0 = (byte)(i&0xff);
		byte b1 = (byte)((i>>8)&0xff);
		byte b2 = (byte)((i>>16)&0xff);
		byte b3 = (byte)((i>>24)&0xff);
		return (((int)b0)<<24) | (((int)b1)<<16) | (((int)b2)<<8) | (int)b3;
	}
	*/
	private static String toHexString(byte[] x)
	{   
		StringBuffer sb = new StringBuffer(x.length * 2);
		for ( int i = 0; i < x.length; i++ )
		{
			byte n = x[i];
			int nibble = (int)(n >> 4)&0xf;
			sb.append ( (char)(nibble >= 10 ? 'a' + nibble - 10 : '0' + nibble) );
			nibble = (int)(n & 0xf);
			sb.append ( (char)(nibble >= 10 ? 'a' + nibble - 10 : '0' + nibble) ); 
		}   
		return sb.toString();
	}   
	public void Server(Data argument, Data result, Manager manager, Session session) throws ProtocolException
	{
		GQueryPasswdArg arg = (GQueryPasswdArg)argument;
		GQueryPasswdRes res = (GQueryPasswdRes)result;
		try 
		{	
			System.out.println("GQueryPasswd:account is "+arg.account.getString()
					+" , login ip is " + arg.loginip);
			//GAuthServer.GetLog().info("GQueryPasswd:account is "+arg.account.getString());
			Object[] userinfo=storage.acquireIdPasswd(arg.account.getString());
			if (userinfo == null) 
			{
				GAuthServer.GetLog().info("GQueryPasswd:can not find user "+arg.account.getString());
				res.retcode=Constants.ERR_INVALID_ACCOUNT; 
				return;
			}
			if (userinfo[0]==null) System.out.println("userinfo[0] is null.");
			if (userinfo[1]==null) System.out.println("userinfo[1] is null.");
			res.response.replace((byte[]) userinfo[1]);
			res.userid	= ((Integer)userinfo[0]).intValue();
			res.retcode = Constants.ERR_SUCCESS;
			//GAuthServer.GetLog().info("GQueryPasswdreturn: retcode="+res.retcode+", userid="+res.userid+"  password="+toHexString(res.response.getBytes()));
			
			//Query forbid information
			Integer userid=new Integer(res.userid);
			storage.deleteTimeoutForbid(userid);

			GAuthServer aum=GAuthServer.GetInstance();
			QueryUserForbid_Re quf=(QueryUserForbid_Re) super.Create("QUERYUSERFORBID_RE");
			quf.userid=userid.intValue();
			quf.list_type = 0; // forbid list at login

			Object[] record_set=storage.acquireForbid(userid);
			///if (record_set == null || record_set.length==0)
			//{
			//	aum.Send(session,quf);
			//	return;
			//}
			//GAuthServer.GetLog().info("Get "+record_set.length+" forbid items for user "+userid);
			boolean bForbidLogin = false;
			if( record_set != null )
			{
				for (int i=0;i<record_set.length;i++)
				{
					Object[] record=(Object[])record_set[i];
					GRoleForbid forbid=new GRoleForbid();
					//userid=((Integer) record[0]).intValue();
					forbid.type=(byte)((Integer)record[1]).intValue();
					if( forbid.type == (byte)Constants.PRV_FORCE_OFFLINE ) bForbidLogin = true;
					forbid.createtime=(int)(((Date)record[2]).getTime()/1000);
					forbid.time=((Integer) record[3]).intValue()+					//forbid time
								forbid.createtime-									//create time
								(int)(Calendar.getInstance().getTime().getTime()/1000);//time now
							
					forbid.reason.replace( (byte[])record[4] );
					GAuthServer.GetLog().info("===userid="+userid+", Forbid type "+forbid.type+", timeleft="+forbid.time);
					quf.forbid.add(forbid);
				}
			}
			if( !bForbidLogin )
			{
				int lockstatus = storage.checkIPLimit(userid, new Integer(bswap(arg.loginip)));
				if( lockstatus == 1 || lockstatus == 2 )
				{
					initReason();
					GRoleForbid forbid = new GRoleForbid();
					forbid.type = (byte)Constants.PRV_FORCE_OFFLINE;
					forbid.createtime = (int)(new Date().getTime()/1000);
					forbid.time = 86400 * 365;
					forbid.reason.replace(lockstatus == 2 ? forbidIPReason : lockReason );
					quf.forbid.add(forbid);
				}	
			}
			aum.Send(session,quf);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}	
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		GQueryPasswdArg arg = (GQueryPasswdArg)argument;
		GQueryPasswdRes res = (GQueryPasswdRes)result;
	}

	public void OnTimeout()
	{
	}

}
