package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.IO.PollIO;
import com.goldhuman.account.storage;

public final class GetAddCashSN extends Rpc
{
	public void Server(Data argument, Data result) throws ProtocolException
	{
		GetAddCashSNArg arg = (GetAddCashSNArg)argument;
		GetAddCashSNRes res = (GetAddCashSNRes)result;
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		GetAddCashSNArg arg = (GetAddCashSNArg)argument;
		GetAddCashSNRes res = (GetAddCashSNRes)result;

		GAuthServer.GetLog().info("GetAddCashSN Client: retcode="+res.retcode+",userid="+res.userid
					+",zoneid="+res.zoneid+",sn="+res.sn );

		if( 0 != res.retcode )
		{
			if( res.retcode >= 1 && res.retcode <= 4 )	res.retcode = -1;
			storage.useCash(new Integer(res.userid), new Integer(res.zoneid),
						new Integer(res.sn), new Integer(-1),
						new Integer(-1), new Integer(-1), new Integer(res.retcode) );
			GAuthServer.GetInstance().SendUseCash_Re( res.userid, res.zoneid, res.retcode );
			return;
		}

		int ret = storage.useCash(new Integer(res.userid), new Integer(res.zoneid),
						new Integer(res.sn), new Integer(-1),
						new Integer(-1), new Integer(-1), new Integer(2) );
		if( ret < 0 )
		{
			GAuthServer.GetInstance().SendUseCash_Re( res.userid, res.zoneid, ret );
			return;
		}

		Object[] rows = storage.getUseCashNow(new Integer(res.userid),
								new Integer(res.zoneid), new Integer(res.sn));
		if( null == rows || rows.length < 1 || null == rows[0]
			|| ((Object[])rows[0]).length < 6 || null == ((Object[])rows[0])[5]  )
		{
			GAuthServer.GetInstance().SendUseCash_Re( res.userid, res.zoneid, -12 );
			return;
		}
		Integer cash = (Integer)(((Object[])rows[0])[5]);

		GAuthServer aum=GAuthServer.GetInstance();
		Session s = aum.GetSessionbyZoneid(new Integer(res.zoneid));

		AddCash ac = (AddCash) Protocol.Create("ADDCASH");
		ac.userid = res.userid;
		ac.zoneid = res.zoneid;
		ac.sn = res.sn;
		ac.cash = cash.intValue();

		ret = storage.useCash(new Integer(res.userid), new Integer(res.zoneid),
						new Integer(res.sn), new Integer(-1),
						new Integer(-1), new Integer(-1), new Integer(3) );
		if( 0 != ret )
		{
			GAuthServer.GetInstance().SendUseCash_Re( res.userid, res.zoneid, ret );
		}
		else if( null != s && aum.Send( s, ac ) )
		{
			PollIO.WakeUp();
		}
		else
		{
			GAuthServer.GetInstance().SendUseCash_Re( res.userid, res.zoneid, -14 );
		}
	}

	public void OnTimeout()
	{
		GetAddCashSNArg arg = (GetAddCashSNArg)argument;
		GAuthServer.GetInstance().SendUseCash_Re( arg.userid, arg.zoneid, -15 );
	}

}
