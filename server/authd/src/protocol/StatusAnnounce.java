package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
public final class StatusAnnounce extends Protocol
{
	public int	userid;
	public int	localsid;
	public byte	status;

	public StatusAnnounce()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(userid);
		os.marshal(localsid);
		os.marshal(status);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		userid = os.unmarshal_int();
		localsid = os.unmarshal_int();
		status = os.unmarshal_byte();
		return os;
	}

	public Object clone()
	{
		try
		{
			StatusAnnounce o = (StatusAnnounce)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		GAuthServer aum= GAuthServer.GetInstance();
		if (aum.zonemap.get(session)==null) return;
		if (aum.aidmap.get(session)==null) return;
		
		int zoneid=((Integer)aum.zonemap.get(session)).intValue();
		int aid=((Integer)aum.aidmap.get(session)).intValue();
		Object[] param = new Object[] {new Integer(zoneid),new Integer(localsid),new Integer(1) };
		storage.recordUserOffline(param,new Integer(userid),new Integer(aid));
		if (param[2]==null) return;
		if (((Integer)param[2]).intValue() == 1)
		{
			GAuthServer.GetLog().info("User "+userid+" logout successfully.");
		}
		else
		{
			GAuthServer.GetLog().info("WARNING: User "+userid+" logout failed. User info is different from DB data.");
		}
	}

}
