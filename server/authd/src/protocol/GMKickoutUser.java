package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
import java.util.*;

public final class GMKickoutUser extends Protocol
{
	public int	gmroleid;
	public int	localsid;
	public int	kickuserid;
	public int	forbid_time;
	public Octets	reason;

	public GMKickoutUser()
	{
		reason = new Octets();
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(gmroleid);
		os.marshal(localsid);
		os.marshal(kickuserid);
		os.marshal(forbid_time);
		os.marshal(reason);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		gmroleid = os.unmarshal_int();
		localsid = os.unmarshal_int();
		kickuserid = os.unmarshal_int();
		forbid_time = os.unmarshal_int();
		os.unmarshal(reason);
		return os;
	}

	public Object clone()
	{
		try
		{
			GMKickoutUser o = (GMKickoutUser)super.clone();
			o.reason = (Octets)reason.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}
	public void Process(Manager manager, Session session) throws ProtocolException
	{
		//query whether the user is online
		Object[] recordset=storage.getUserOnlineInfo(new Integer(kickuserid));
		GAuthServer aum=GAuthServer.GetInstance();
		if (recordset!=null && recordset.length!=0 && forbid_time>=0)
		{
			for (int i=0;i<recordset.length;i++)
			{
				Object[] login_info=(Object[]) recordset[i];
				Integer zoneid,localsid,aid;
				zoneid=(Integer) login_info[0];
				localsid=(Integer) login_info[1];
				aid=(Integer) login_info[2];
				if ( zoneid!=null && localsid!=null && aid!=null ) //user is online
				{
					KickoutUser ku=(KickoutUser) super.Create("KICKOUTUSER");
					ku.userid=kickuserid;
					ku.localsid=localsid.intValue();
					ku.cause=Constants.ERR_KICKOUT;	
					GAuthServer.GetLog().info("GMKickoutUser: Send Kickout userid="+ku.userid+" sid="+ku.localsid+" zoneid="+zoneid+" aid="+aid);
					Session sid= aum.GetSessionbyZoneid(zoneid);
					if (sid!=null)
						GAuthServer.GetInstance().Send( sid,ku );
					//remove online record of that user
					Object[] param = new Object[] { zoneid,localsid,new Integer(1) };
					storage.recordUserOffline(param,new Integer(kickuserid),aid);
				}
			}
		}
		//record forbid login info
		GAuthServer.GetLog().info("Add PRV_FORCE_OFFLINE to user "+kickuserid+" for "+forbid_time+" seconds. localsid="+localsid);
		if( -2 != localsid )
			storage.addForbid(new Integer(kickuserid),new Integer(Constants.PRV_FORCE_OFFLINE),new Integer(forbid_time), reason.array(),new Integer(gmroleid));
		//announce new forbid info to all delivery
		QueryUserForbid_Re quf=(QueryUserForbid_Re) super.Create("QUERYUSERFORBID_RE");
        quf.userid=kickuserid;
		quf.list_type = 1; // reply of forbid
		GRoleForbid forbid=new GRoleForbid();
		forbid.type=(byte)Constants.PRV_FORCE_OFFLINE;
		forbid.createtime=(int)Calendar.getInstance().getTime().getTime()/1000;
		forbid.time=forbid_time;
		if ( forbid.time<=0 ) forbid.time=0;
		forbid.reason=reason;
		quf.forbid.add(forbid);
		Iterator<Session> it=(Iterator<Session>)aum.zonemap.keySet().iterator();
		while ( it.hasNext() )
			aum.Send( (Session)it.next(),quf );
	}

}
