package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;

public final class QueryUserPrivilege2 extends Protocol
{
	public int	userid;
	public int	zoneid;

	public QueryUserPrivilege2()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(userid);
		os.marshal(zoneid);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		userid = os.unmarshal_int();
		zoneid = os.unmarshal_int();
		return os;
	}

	public Object clone()
	{
		try
		{
			QueryUserPrivilege2 o = (QueryUserPrivilege2)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		//get user's privilege
		Object[] prv=storage.acquireUserPrivilege(new Integer(userid),new Integer(zoneid));
		if (prv==null) return;
		//send to delivery
		QueryUserPrivilege_Re qup=(QueryUserPrivilege_Re)super.Create("QUERYUSERPRIVILEGE_RE");
		qup.userid=userid;
			//test code
		Object[] row=null;
		for (int i=0;i<prv.length;i++)
		{
			row=(Object[])prv[i];
			if (row != null && ((Integer)row[0]).intValue()<255 )
				qup.auth.add( new MByte((byte) ((Integer)row[0]).intValue()) );
		}
		GAuthServer aum=GAuthServer.GetInstance();
		aum.Send(session,qup);
	}

}
