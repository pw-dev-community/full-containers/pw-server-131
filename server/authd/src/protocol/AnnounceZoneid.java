package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
public final class AnnounceZoneid extends Protocol
{
	public byte	zoneid;
	public byte aid;
	public byte blreset;
	public AnnounceZoneid()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(zoneid);
		os.marshal(aid);
		os.marshal(blreset);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		zoneid = os.unmarshal_byte();
		aid = os.unmarshal_byte();
		blreset = os.unmarshal_byte();
		return os;
	}

	public Object clone()
	{
		try
		{
			AnnounceZoneid o = (AnnounceZoneid)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		GAuthServer aum = GAuthServer.GetInstance();
		Integer _zoneid,_aid;
		_zoneid=new Integer((int)(0xFF & (int)zoneid));
		_aid=new Integer((int)(0xFF & (int)aid));
		if (blreset!=0)
		{
			//System.out.println("<<< clear all online records on zone "+_zoneid+" >>>");
			GAuthServer.GetLog().info("<<< clear all online records on zone "+_zoneid+" >>>");
			storage.clearOnlineRecords(_zoneid,_aid);
		}
		aum.zonemap.put(session,_zoneid);
		aum.aidmap.put(session,_aid);
		//System.out.println("zone "+_zoneid+"  aid "+_aid+" announced.");
		GAuthServer.GetLog().info("zone "+_zoneid+"  aid "+_aid+" announced.");
	}

}
