package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;

public final class GMShutup extends Protocol
{
	public int	gmroleid;
	public int	localsid;
	public int	dstuserid;
	public int	forbid_time;
	public Octets	reason;

	public GMShutup()
	{
		reason = new Octets();
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(gmroleid);
		os.marshal(localsid);
		os.marshal(dstuserid);
		os.marshal(forbid_time);
		os.marshal(reason);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		gmroleid = os.unmarshal_int();
		localsid = os.unmarshal_int();
		dstuserid = os.unmarshal_int();
		forbid_time = os.unmarshal_int();
		os.unmarshal(reason);
		return os;
	}

	public Object clone()
	{
		try
		{
			GMShutup o = (GMShutup)super.clone();
			o.reason = (Octets)reason.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		System.out.println("Add PRV_FORBID_TALK to user "+dstuserid+" for "+forbid_time+" seconds.");
		storage.addForbid(new Integer(dstuserid),new Integer(Constants.PRV_FORBID_TALK),new Integer(forbid_time),
				reason.array(),new Integer(gmroleid));
	}

}
