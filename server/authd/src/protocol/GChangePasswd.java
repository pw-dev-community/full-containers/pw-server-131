package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;

import com.goldhuman.account.storage;

public final class GChangePasswd extends Rpc
{
	public void Server(Data argument, Data result) throws ProtocolException
	{
		GChangePasswdArg arg = (GChangePasswdArg)argument;
		GChangePasswdRes res = (GChangePasswdRes)result;
		res.retcode = -1;
		try {
			res.username = arg.username;
			res.newpwd = arg.newpwd;

			if( storage.changePasswdWithOld(arg.username.getString(), arg.newpwd.getString(),
				arg.oldpwd.getString()) )
				res.retcode = Constants.ERR_SUCCESS;
		} catch (Exception ex) { ex.printStackTrace(System.out); }
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		GChangePasswdArg arg = (GChangePasswdArg)argument;
		GChangePasswdRes res = (GChangePasswdRes)result;
	}

	public void OnTimeout()
	{
	}

}
