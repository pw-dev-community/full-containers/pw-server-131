package protocol;

import java.util.*;
import com.goldhuman.IO.*;
import com.goldhuman.Common.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.PollIO;
import com.goldhuman.account.storage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
public final class GAuthServer extends Manager
{
	static {
		new java.util.Timer().schedule(new java.util.TimerTask() {
				public void run()
				{
					try
					{
						GAuthServer aum=GAuthServer.GetInstance();
						Object[] rows = null;

						// 0.queue
						rows = storage.getUseCashNow(new Integer(0));
						if( null != rows )
						{
							for( int i=0; i<rows.length; i++ )
							{
								Object[] row = (Object[])rows[i];
								Integer userid = (Integer)row[0];
								Integer zoneid = (Integer)row[1];
								Integer sn = (Integer)row[2];

								int ret = storage.useCash(new Integer(userid), new Integer(zoneid),
										new Integer(sn), new Integer(-1),
										new Integer(-1), new Integer(-1), new Integer(0) );
								if( 0 == ret )
								{
									Session s = aum.GetSessionbyZoneid(zoneid);
									GetAddCashSNArg rpcarg = new GetAddCashSNArg();
									rpcarg.userid = userid;
									rpcarg.zoneid = zoneid;
									GetAddCashSN rpc = (GetAddCashSN)Rpc.Call("GetAddCashSN", rpcarg);
									if( null != s && aum.Send( s,rpc ) )
									{
										PollIO.WakeUp();
										java.lang.Thread.sleep( 5000 );
									}
								}
								GAuthServer.GetLog().info("UseCashTimerTask: status=0,userid="+userid
											+",zoneid="+zoneid+",sn="+sn+",ret="+ret );
							}
						}

						// 1.create ok
						rows = storage.getUseCashNow(new Integer(1));
						if( null != rows )
						{
							for( int i=0; i<rows.length; i++ )
							{
								Object[] row = (Object[])rows[i];
								Integer userid = (Integer)row[0];
								Integer zoneid = (Integer)row[1];
								Integer sn = (Integer)row[2];

								Session s = aum.GetSessionbyZoneid(zoneid);
								GetAddCashSNArg rpcarg = new GetAddCashSNArg();
								rpcarg.userid = userid;
								rpcarg.zoneid = zoneid;
								GetAddCashSN rpc = (GetAddCashSN)Rpc.Call("GetAddCashSN", rpcarg);
								if( null != s && aum.Send( s,rpc ) )
									PollIO.WakeUp();
								GAuthServer.GetLog().info("UseCashTimerTask: status=1,userid="+userid
											+",zoneid="+zoneid+",sn="+sn+",session="+s );
							}
						}

						// 2.get sn ok
						rows = storage.getUseCashNow(new Integer(2));
						if( null != rows )
						{
							for( int i=0; i<rows.length; i++ )
							{
								Object[] row = (Object[])rows[i];
								Integer userid = (Integer)row[0];
								Integer zoneid = (Integer)row[1];
								Integer sn = (Integer)row[2];

								int	ret = storage.useCash(new Integer(userid), new Integer(zoneid),
											new Integer(sn), new Integer(-1),
											new Integer(-1), new Integer(-1), new Integer(3) );
								GAuthServer.GetLog().info("UseCashTimerTask: status=2,userid="+userid
											+",zoneid="+zoneid+",sn="+sn+",ret="+ret );
							}
						}

						// 3.send add cash ok
						rows = storage.getUseCashNow(new Integer(3));
						if( null != rows )
						{
							for( int i=0; i<rows.length; i++ )
							{
								Object[] row = (Object[])rows[i];
								Integer userid = (Integer)row[0];
								Integer zoneid = (Integer)row[1];
								Integer sn = (Integer)row[2];
								Integer cash = (Integer)row[5];

								Session s = aum.GetSessionbyZoneid(zoneid);
								AddCash ac = (AddCash) Protocol.Create("ADDCASH");
								ac.userid = userid;
								ac.zoneid = zoneid;
								ac.sn = sn;
								ac.cash = cash;
								if( null != s && aum.Send( s, ac ) )
									PollIO.WakeUp();
								GAuthServer.GetLog().info("UseCashTimerTask: status=3,userid="+userid
											+",zoneid="+zoneid+",sn="+sn+",session="+s );
							}
						}
					} catch(Exception ex) { ex.printStackTrace(); }
				}
			},1000*300,1000*300);
	}
	
	private static final Log log = LogFactory.getLog(GAuthServer.class);
	//private static final MyLog mylog= new MyLog();
	private GAuthServer() { }
	
	protected static Map usermap=new HashMap();	/* userid to {zoneid,roleid} map */
	protected static Map zonemap= Collections.synchronizedMap(new HashMap()); 	/* SessionID to zoneid map */
	protected static Map aidmap= Collections.synchronizedMap(new HashMap()); 	/* SessionID to accounting area id map */
	protected static Map accntmap=new HashMap(); 	/* userid to timestamp map */
	protected static Manager manager=null;
	protected static Session session=null;

	private static GAuthServer instance=new GAuthServer();
			
	protected void OnAddSession(Session session)
	{
		System.out.println("GAuthServer::OnAddSession " + session);
	}

	protected void OnDelSession(Session session)
	{
		//System.out.println("GAuthServer::nDelSession " + session);
		GAuthServer.GetLog().info("GAuthServer::nDelSession " + session);
		Integer zoneid=(Integer) zonemap.get(session);
		Integer aid=(Integer) aidmap.get(session);
		if (zoneid!=null && aid!=null)
		{
			GAuthServer.GetLog().info("GAuthServer::OnDelSession " + session + "zoneid="+zoneid+", aid="+aid);
		}
		else
		{
			GAuthServer.GetLog().info("GAuthServer::OnDelSession " + session);
		}
		zonemap.remove(session);
		aidmap.remove(session);
	}

	protected State GetInitState()
	{
		return State.Get("GAuthServer");
	}

	protected String Identification()
	{
		return "GAuthServer";
	}

	public Octets shared_key;
	public static GAuthServer GetInstance()
	{
		return instance;
	}
	public static Log GetLog()
	{
		return log;
	}

	public Session GetSessionbyZoneid(Integer zoneid)
	{
		GAuthServer aum=GAuthServer.GetInstance();
		if (!aum.zonemap.containsValue(zoneid)) return null;
		Set entryset = aum.zonemap.entrySet();
		Iterator it=entryset.iterator();
		Map.Entry map_entry;
		while (it.hasNext())
		{
			map_entry =(Map.Entry) it.next();
			if (((Integer)map_entry.getValue()).intValue() == zoneid.intValue())
				return (Session)map_entry.getKey();
		}
		return null;
	}

	public void SetUseCashSession( Manager manager, Session session )
	{
		this.manager = manager;
		this.session = session;
	} 

	public boolean SendUseCash_Re( int userid, int zoneid, int retcode )
	{
		GAuthServer.GetLog().info("SendUseCash_Re: retcode="+retcode+",userid="+userid+",zoneid="+zoneid );

		if( null == manager || null == session )
			return false;
		UseCash_Re re = (UseCash_Re) Protocol.Create("USECASH_RE");
		re.retcode = retcode;
		re.userid = userid;
		re.zoneid = zoneid;
		return manager.Send( session, re );
	}

}
