package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;

public final class UseCashArg extends Rpc.Data
{
	public int	zoneid;
	public int	userid;
	public int	aid;
	public int	point;
	public int	cash;

	public UseCashArg()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(zoneid);
		os.marshal(userid);
		os.marshal(aid);
		os.marshal(point);
		os.marshal(cash);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		zoneid = os.unmarshal_int();
		userid = os.unmarshal_int();
		aid = os.unmarshal_int();
		point = os.unmarshal_int();
		cash = os.unmarshal_int();
		return os;
	}

	public Object clone()
	{
		try
		{
			UseCashArg o = (UseCashArg)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

}
