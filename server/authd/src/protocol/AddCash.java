package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;

public final class AddCash extends Protocol
{
	public int	userid;
	public int	zoneid;
	public int	sn;
	public int	cash;

	public AddCash()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(userid);
		os.marshal(zoneid);
		os.marshal(sn);
		os.marshal(cash);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		userid = os.unmarshal_int();
		zoneid = os.unmarshal_int();
		sn = os.unmarshal_int();
		cash = os.unmarshal_int();
		return os;
	}

	public Object clone()
	{
		try
		{
			AddCash o = (AddCash)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
	}

}
