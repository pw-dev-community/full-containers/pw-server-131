package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;

public final class QueryPasswd extends Rpc
{
	public void Server(Data argument, Data result) throws ProtocolException
	{
		QueryPasswdArg arg = (QueryPasswdArg)argument;
		QueryPasswdRes res = (QueryPasswdRes)result;
		try 
		{	
			GAuthServer.GetLog().info("Receive QueryPasswd for user "+arg.account.getString());
			Object[] userinfo=storage.acquireIdPasswd(arg.account.getString());
			if (userinfo == null) {res.retcode=Constants.ERR_COMMUNICATION; return;}

			res.retcode = Constants.ERR_SUCCESS;
			res.userid 	= ((Integer)userinfo[0]).intValue();
			res.password.replace((byte[])userinfo[1]);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		QueryPasswdArg arg = (QueryPasswdArg)argument;
		QueryPasswdRes res = (QueryPasswdRes)result;
	}

	public void OnTimeout()
	{
	}

}
