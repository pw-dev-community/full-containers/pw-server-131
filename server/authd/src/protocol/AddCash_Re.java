package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;

public final class AddCash_Re extends Protocol
{
	public int	retcode;
	public int	userid;
	public int	zoneid;
	public int	sn;

	public AddCash_Re()
	{
	}

	public OctetsStream marshal(OctetsStream os)
	{
		os.marshal(retcode);
		os.marshal(userid);
		os.marshal(zoneid);
		os.marshal(sn);
		return os;
	}

	public OctetsStream unmarshal(OctetsStream os) throws MarshalException
	{
		retcode = os.unmarshal_int();
		userid = os.unmarshal_int();
		zoneid = os.unmarshal_int();
		sn = os.unmarshal_int();
		return os;
	}

	public Object clone()
	{
		try
		{
			AddCash_Re o = (AddCash_Re)super.clone();
			return o;
		}
		catch (Exception e) { }
		return null;
	}

	public void Process(Manager manager, Session session) throws ProtocolException
	{
		GAuthServer.GetLog().info("AddCash_Re: retcode="+retcode+",userid="+userid
					+",zoneid="+zoneid+",sn="+sn );

		if( 0 != retcode )
		{
			if( retcode >= 1 && retcode <= 4 )	retcode = -1;
			storage.useCash(new Integer(userid), new Integer(zoneid),
						new Integer(sn), new Integer(-1),
						new Integer(-1), new Integer(-1), new Integer(retcode) );
			GAuthServer.GetInstance().SendUseCash_Re( userid, zoneid, retcode );
			return;
		}

		int ret = storage.useCash(new Integer(userid), new Integer(zoneid),
						new Integer(sn), new Integer(-1),
						new Integer(-1), new Integer(-1), new Integer(4) );
		GAuthServer.GetInstance().SendUseCash_Re( userid, zoneid, ret );
	}

}
