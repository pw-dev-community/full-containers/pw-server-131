package protocol;

import com.goldhuman.Common.*;
import com.goldhuman.Common.Marshal.*;
import com.goldhuman.Common.Security.*;
import com.goldhuman.IO.Protocol.*;
import com.goldhuman.account.storage;
public final class UserLogout extends Rpc
{
	public void Server(Data argument, Data result, Manager manager, Session session) throws ProtocolException
	{
		UserLogoutArg arg = (UserLogoutArg)argument;
		UserLogoutRes res = (UserLogoutRes)result;

		GAuthServer aum=GAuthServer.GetInstance();
        if (aum.zonemap.get(session)==null) { res.retcode=Constants.ERR_LOGOUT_FAIL; return; }
        if (aum.aidmap.get(session)==null) { res.retcode=Constants.ERR_LOGOUT_FAIL; return;  }		
		int zoneid=((Integer)aum.zonemap.get(session)).intValue();
		int aid=((Integer)aum.aidmap.get(session)).intValue();
		
		Object[] param = new Object[] { new Integer(zoneid),new Integer(arg.localsid),new Integer(1) };
		storage.recordUserOffline(param,new Integer(arg.userid),new Integer(aid));
		if (param==null || param[2]==null) { res.retcode=Constants.ERR_LOGOUT_FAIL; return; }
		
        if (((Integer)param[2]).intValue() == 1)
		{
			GAuthServer.GetLog().info("UserLogout::User "+arg.userid+" logout successfully.");
			res.retcode=Constants.ERR_SUCCESS;
		}
		else
		{
			GAuthServer.GetLog().info("UserLogout::WARNING: User "+arg.userid+" logout failed. User info is different from DB data.");
			res.retcode=Constants.ERR_LOGOUT_FAIL;
		}		
	}

	public void Client(Data argument, Data result) throws ProtocolException
	{
		UserLogoutArg arg = (UserLogoutArg)argument;
		UserLogoutRes res = (UserLogoutRes)result;
	}

	public void OnTimeout()
	{
	}

}
